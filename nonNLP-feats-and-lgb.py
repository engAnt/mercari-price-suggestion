
# ## __Goal__
# * [done] use cleaned data to run LightGBM data
# * [done] investigate feature importance
# * [done] carry out hyperparameter tuning
# * [done] carry out recursive feature importance
# * [done] do CV

#****************************************************************************
##   import a few libraries, system checks
#****************************************************************************
import numpy as np
import pandas as pnd
import gc
from time import time
import random
random_seed = 3
random.seed(random_seed)

import lightgbm as lgb
import sys
print("OS: ", sys.platform)
print("Python: ", sys.version)
print("Numpy: ", np.__version__)
print("Pandas: ", pnd.__version__)
print("LightGBM: ", lgb.__version__)
print("numpy, pandas, and lightgbm libs imported...")

from sklearn.preprocessing import LabelEncoder, MinMaxScaler
import re
from sklearn.model_selection import train_test_split
#import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
print("sklearn libs imported...")

#****************************************************************************
##       useful functions
#****************************************************************************
data_loc = "./data/"   # replace when running in kernel
def import_data():
    t_start = time()
    tr = pnd.read_table(data_loc +"train.tsv")
    te = pnd.read_table(data_loc +"test.tsv")
    print("Data imported...")
    print("train shape:", tr.shape)
    print("test shape:", te.shape)
    print("import_data() exution time:", time() - t_start, "seconds")
    return (tr, te)

def expand_category(df_data):
    ### --- extract sub categories
    t_start = time()
    tmp_df = df_data
    ## split "category_name"
    tmp_cat_df = tmp_df.category_name.str.split("/", expand=True)
    cat_levels = ["cat_name", "cat_name2", "cat_name3", "cat_name4", "cat_name5"]
    tmp_cat_df.columns = cat_levels
    tmp_df = tmp_df.assign(cat_name = tmp_cat_df.cat_name)
    tmp_df = tmp_df.assign(cat_name2 = tmp_cat_df.cat_name2)
    tmp_df = tmp_df.assign(cat_name3 = tmp_cat_df.cat_name3)
    print("expand_category() execution time:", time() - t_start, "seconds")
    return tmp_df

def missing_indicators(df_data):
    ### --- create missing indicator variables
    t_start = time()
    tmp_df = df_data
    tmp_df = tmp_df.assign(brand_na = tmp_df.brand_name.isnull().astype(int))
    print("executing missing indicators took:", time() - t_start, "seconds")
    return tmp_df

def replace_missing(df_data):
    ### --- replace missing values
    t_start = time()
    tmp_df = df_data
    str_na="NA"
    tmp_df.category_name = tmp_df.category_name.fillna(value=str_na)
    tmp_df.brand_name = tmp_df.brand_name.fillna(value=str_na)
    tmp_df.item_description = tmp_df.item_description.fillna(value=str_na)
    tmp_df.cat_name = tmp_df.cat_name.fillna(value=str_na)
    tmp_df.cat_name2 = tmp_df.cat_name2.fillna(value=str_na)
    tmp_df.cat_name3 = tmp_df.cat_name3.fillna(value=str_na)
    ##[rfeCV] tmp_df.cat_name4 = tmp_df.cat_name4.fillna(value=str_na)
    ##[rfeCV] tmp_df.cat_name5 = tmp_df.cat_name5.fillna(value=str_na)
    print("replacing missing values took:", time() - t_start, "seconds")
    return tmp_df

## special replacement for item_description
def replaceNoDescr(str_val):
    str_lower = str(str_val).lower()
    yet_lst = ["no description yet", "no description ye", "no description y"]
    if str_lower in yet_lst and len(str_lower) in [16, 17, 18]:
        return "NA"
    else:
        return str_val

### --- add meta variables
def get_descr_meta(dataf):
    t_meta = time()
    tmp_df = dataf
    tmp_series = tmp_df.item_description   # extract from 'item_description'  
    tmp_df = tmp_df.assign(desc_len = tmp_series.str.len())
    tmp_df = tmp_df.assign(desc_tot_excla = tmp_series.str.count("[!]"))
    tmp_df = tmp_df.assign(desc_tot_qmark = tmp_series.str.count("[?]"))
    tmp_df = tmp_df.assign(desc_tot_alnum = tmp_series.str.count("\\w"))
    tmp_df = tmp_df.assign(desc_tot_upper = tmp_series.str.count("[A-Z]"))
    tmp_df = tmp_df.assign(desc_tot_lower = tmp_series.str.count("[a-z]"))
    tmp_df = tmp_df.assign(desc_tot_digit = tmp_series.str.count("[0-9]"))
    tmp_df = tmp_df.assign(desc_tot_space = tmp_series.str.count("[' ']"))
    tmp_df = tmp_df.assign(desc_tot_hyphen = tmp_series.str.count("[-]"))
    tmp_df = tmp_df.assign(desc_tot_word = tmp_series.str.count("[A-Za-z]{3,}"))
    tmp_df = tmp_df.assign(desc_tot_rm = tmp_series.str.count("\[rm]"))
    print("meta features from description took:", time() - t_meta, "seconds")
    return tmp_df

def get_name_meta(dataf):
    t_meta = time()
    tmp_df = dataf
    tmp_series = tmp_df.name   # extract from 'name'    
    tmp_df = tmp_df.assign(name_len = tmp_series.str.len())
    tmp_df = tmp_df.assign(name_tot_excla = tmp_series.str.count("[!]"))
    ##[rfeCV] tmp_df = tmp_df.assign(name_tot_qmark = tmp_series.str.count("[?]"))
    tmp_df = tmp_df.assign(name_tot_alnum = tmp_series.str.count("\\w"))
    tmp_df = tmp_df.assign(name_tot_upper = tmp_series.str.count("[A-Z]"))
    tmp_df = tmp_df.assign(name_tot_lower = tmp_series.str.count("[a-z]"))
    tmp_df = tmp_df.assign(name_tot_digit = tmp_series.str.count("[0-9]"))
    tmp_df = tmp_df.assign(name_tot_space = tmp_series.str.count("[' ']"))
    tmp_df = tmp_df.assign(name_tot_hyphen = tmp_series.str.count("[-]"))
    tmp_df = tmp_df.assign(name_tot_word = tmp_series.str.count("[A-Za-z]{3,}"))
    tmp_df = tmp_df.assign(name_tot_rm = tmp_series.str.count("\[rm]"))
    print("meta features from name took:", time() - t_meta, "seconds")
    return tmp_df


### --- encode categorical variables
## use feature hashing???
## function(s) to help clean text a bit, so that
## 'J. Crew' and 'J.Crew' are the same thing
def cleanTextHelper(text_val):
    res_txt = re.split("[^\\w\\.&/]", str(text_val))
    res_txt = ''.join(res_txt)
    return res_txt

def cleanText(a_series):
    return a_series.apply(cleanTextHelper)

def category_encode(df_train, df_test):
    t_start = time()
    tmp_train, tmp_test = df_train, df_test
    ## initialize label encoder
    le = LabelEncoder()
    ## "category_name"
    concat_train = cleanText(tmp_train.category_name)
    concat_test = cleanText(tmp_test.category_name)
    le.fit(np.concatenate((concat_train, concat_test), axis=0))
    tmp_train.category_name = le.transform(concat_train)
    tmp_test.category_name = le.transform(concat_test)
    ## "brand_name"
    concat_train = cleanText(tmp_train.brand_name)
    concat_test = cleanText(tmp_test.brand_name)
    le.fit(np.concatenate((concat_train, concat_test), axis=0))
    tmp_train.brand_name = le.transform(concat_train)
    tmp_test.brand_name = le.transform(concat_test)
    ## "cat_name"
    concat_train = cleanText(tmp_train.cat_name)
    concat_test = cleanText(tmp_test.cat_name)
    le.fit(np.concatenate((concat_train, concat_test), axis=0))
    tmp_train.cat_name = le.transform(concat_train)
    tmp_test.cat_name = le.transform(concat_test)
    ## "cat_name2"
    concat_train = cleanText(tmp_train.cat_name2)
    concat_test = cleanText(tmp_test.cat_name2)
    le.fit(np.concatenate((concat_train, concat_test), axis=0))
    tmp_train.cat_name2 = le.transform(concat_train)
    tmp_test.cat_name2 = le.transform(concat_test)
    ## "cat_name3"
    concat_train = cleanText(tmp_train.cat_name3)
    concat_test = cleanText(tmp_test.cat_name3)
    le.fit(np.concatenate((concat_train, concat_test), axis=0))
    tmp_train.cat_name3 = le.transform(concat_train)
    tmp_test.cat_name3 = le.transform(concat_test)
    del le
    print("label encoding took:", time() - t_start, "seconds")
    return (tmp_train, tmp_test)

def print_a_row(df_train, df_test):
    ### --- take a peek at both train & test
    print('*' * 78)
    print('*' * 78)
    print("*** Train **")
    print("features:\n", list(df_train.columns.values))
    print("first row:\n", list(df_train.loc[1,:].values))
    print('*' * 78)
    print("*** Test **")
    print("features:\n", list(df_test.columns.values))
    print("first row:\n", list(df_test.loc[1,:].values))
    print('*' * 78)
    print("train shape:", df_train.shape)
    print("test shape:", df_test.shape)


train, test = import_data()

## sub categories
train = expand_category(train)
test = expand_category(test)

## add missing indicators
train = missing_indicators(train)
test = missing_indicators(test)

## replace missing values with 'NA'
train = replace_missing(train)
test = replace_missing(test)

## categorical encoding
train, test = category_encode(train, test)


## special replacement for item_description
t_no_descr = time()
train.item_description = train.item_description.apply(replaceNoDescr)
test.item_description = test.item_description.apply(replaceNoDescr)
print("special item description replacement took:", time() - t_no_descr, "seconds")


## meta features from 'item_description'
train = get_descr_meta(train)
test = get_descr_meta(test)

## meta features from 'name'
train = get_name_meta(train)
test = get_name_meta(test)


## prepare train and test for training
train_feats = train.columns.difference(["price", "train_id", "name", "item_description"])
trainSelX = train.loc[:, train_feats]
#trainSelY = train['price'].values

train = train.assign(target = np.log1p(train.price))
target_scaler = MinMaxScaler(feature_range=(-1, 1))
#train["target"] = target_scaler.fit_transform(train.target.values.reshape(-1, 1))
price_target = target_scaler.fit_transform(train.target.values.reshape(-1, 1))

#trainSelY = train.target.values
trainSelY = price_target#.values

## extract holdout for independent evaluation
trainX, testX, trainY, testY = train_test_split(trainSelX, trainSelY, test_size=0.01, random_state=random_seed)

## Scale 
scaler = MinMaxScaler(feature_range=(0, 1))
scaler.fit(trainX)
trainX = scaler.transform(trainX)   # transform train
trainX = pnd.DataFrame(trainX, columns=train_feats)
testX = scaler.transform(testX)   # transform evaluation set
testX = pnd.DataFrame(testX, columns=train_feats)


## select train/validation splits from train
trainX, validateX, trainY, validateY = train_test_split(trainX, trainY, test_size=0.01, random_state=random_seed)

print_format_text = "{0} split X shape: {1}, {0} split Y shape: {2}"
print(print_format_text.format("train", trainX.shape, trainY.shape))
print(print_format_text.format("validate", validateX.shape, validateY.shape))
print(print_format_text.format("test", testX.shape, testY.shape))


## first delete train and test to make space on memory
del train
del test
gc.collect()


lgb_train = lgb.Dataset(trainX, label=np.ravel(trainY))
lgb_eval = lgb.Dataset(validateX, label=np.ravel(validateY), reference=lgb_train)

params_1 = {
    "task": "train",
    "boosting_type": "gbdt",
    "metric": {"rmse"},
    "objective": "regression",
    "learning_rate": 0.24,
    "max_depth": 8,
    "num_leaves": 210,
    "nthread": 3
}

lgb_model = lgb.train(params_1, train_set=lgb_train, num_boost_round=2500,                   valid_sets=[lgb_train, lgb_eval], valid_names=["train", "eval"],                   early_stopping_rounds=12, verbose_eval=62, feature_name="auto")


print(lgb_model.best_iteration)
preds_eval = lgb_model.predict(testX, num_iteration=lgb_model.best_iteration)
eval_rmse = mean_squared_error(testY, preds_eval) ** 0.5
print("evaludation rmse is:", eval_rmse)


feat_imp = pnd.Series(index=lgb_model.feature_name(), data=lgb_model.feature_importance())
feat_imp.sort_values(ascending=False)


#lgb.cv(params_1, train_set=lgb_train, num_boost_round=2500, nfold=5, stratified=False, \
#                   shuffle=True, early_stopping_rounds=12, verbose_eval=62, feature_name="auto", \
#                               seed=random_seed)


