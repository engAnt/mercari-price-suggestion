
#****************************************************************************
##   import a few libraries, system checks
#****************************************************************************
import numpy as np
import pandas as pnd
from time import time
import random
random_seed = 3
random.seed(random_seed)

import mxnet as mx
from mxnet import nd, autograd, gluon
import os
os.environ['MXNET_CPU_WORKER_NTHREADS']='4'
import sys
print("OS: ", sys.platform)
print("Python: ", sys.version)
print("Numpy: ", np.__version__)
print("Pandas: ", pnd.__version__)
print("MXNet: ", mx.__version__)
print("numpy, pandas, mxnet, time and random libraries imported...")

#****************************************************************************
##   load data
#****************************************************************************
def import_data():
    t_start = time()
    tr = pnd.read_table("../input/train.tsv")
    te = pnd.read_table("../input/test.tsv")
    print("Data imported...")
    print("train shape:", tr.shape)
    print("test shape:", te.shape)
    print("import_data() execution time:", time() - t_start, "seconds")
    return (tr, te)

train, test = import_data()

#****************************************************************************
##   clean, transform data
#****************************************************************************
def expand_category(dataf):
    t_start = time()
    tmp_df = dataf
    ## split "category_name"
    tmp_cat_df = tmp_df.category_name.str.split("/", expand=True)
    cat_levels = ["cat_name", "cat_name2", "cat_name3", "cat_name4", "cat_name5"]
    tmp_cat_df.columns = cat_levels
    ## merge w/ train, where unique value is not just None
    tmp_df = tmp_df.assign(cat_name = tmp_cat_df.cat_name)
    tmp_df = tmp_df.assign(cat_name2 = tmp_cat_df.cat_name2)
    tmp_df = tmp_df.assign(cat_name3 = tmp_cat_df.cat_name3)
    tmp_df = tmp_df.assign(cat_name4 = tmp_cat_df.cat_name4)
    tmp_df = tmp_df.assign(cat_name5 = tmp_cat_df.cat_name5)
    print("expand_category() exeution time:", time() - t_start, "seconds")
    return tmp_df

## => Train
train = expand_category(train)
## => Test
test = expand_category(test)


##*** missing indicator variables **
t_start = time()
## indicate brand_name is missing
train = train.assign(brand_na = train.brand_name.isnull().astype(int))
test = test.assign(brand_na = test.brand_name.isnull().astype(int))
## indicate category_name is missing
train = train.assign(category_na = train.category_name.isnull().astype(int))
test = test.assign(category_na = test.category_name.isnull().astype(int))
## indicate item_description is missing
train = train.assign(description_na = train.item_description.isnull().astype(int))
test = test.assign(description_na = test.item_description.isnull().astype(int))
## indicate cat_name is missing
train = train.assign(cat_name_na = train.cat_name.isnull().astype(int))
test = test.assign(cat_name_na = test.cat_name.isnull().astype(int))
## indicate cat_name2 is missing
train = train.assign(cat_name2_na = train.cat_name2.isnull().astype(int))
test = test.assign(cat_name2_na = test.cat_name2.isnull().astype(int))
## indicate cat_name3 is missing
train = train.assign(cat_name3_na = train.cat_name3.isnull().astype(int))
test = test.assign(cat_name3_na = test.cat_name3.isnull().astype(int))
## indicate cat_name4 is missing
train = train.assign(cat_name4_na = train.cat_name4.isnull().astype(int))
test = test.assign(cat_name4_na = test.cat_name4.isnull().astype(int))
## indicate cat_name5 is missing
train = train.assign(cat_name5_na = train.cat_name5.isnull().astype(int))
test = test.assign(cat_name5_na = test.cat_name5.isnull().astype(int))
print("executing missing value indicators took:", time() - t_start, "seconds")


##*** replace missing values **
t_start = time()
# category_name
train.category_name.fillna(value="NA", inplace=True)
test.category_name.fillna(value="NA", inplace=True)
# brand_name
train.brand_name.fillna(value="NA", inplace=True)
test.brand_name.fillna(value="NA", inplace=True)
# item_description - just in case the new test file has a lot of missing descriptions
train.item_description.fillna(value="NA", inplace=True)
test.item_description.fillna(value="NA", inplace=True)
# cat_name
train.cat_name.fillna(value="NA", inplace=True)
test.cat_name.fillna(value="NA", inplace=True)
# cat_name2
train.cat_name2.fillna(value="NA", inplace=True)
test.cat_name2.fillna(value="NA", inplace=True)
# cat_name3
train.cat_name3.fillna(value="NA", inplace=True)
test.cat_name3.fillna(value="NA", inplace=True)
# cat_name4
train.cat_name4.fillna(value="NA", inplace=True)
test.cat_name4.fillna(value="NA", inplace=True)
# cat_name5
train.cat_name5.fillna(value="NA", inplace=True)
test.cat_name5.fillna(value="NA", inplace=True)
print("replacing missing values took:", time() - t_start, "seconds")


##*** encode categorical variables **
## use feature hashing???
## function(s) to help clean text a bit, so that
## 'J. Crew' and 'J.Crew' are the same thing
from sklearn.preprocessing import LabelEncoder
import re
def cleanTextHelper(text_val):
    #t_start = time()
    res_txt = re.split("[^\\w\\.&/]", text_val)
    res_txt = ''.join(res_txt)
    #print("cleanTextHelper() took:", time() - t_start, "seconds")
    return res_txt

# def cleanText(aSeries, useUnique=True):
#     #t_start = time()
#     my_series = aSeries
#     if useUnique:
#         my_series = pnd.Series(aSeries.unique())
#     #print("cleanText() took:", time() - t_start, "seconds")
#     return my_series.apply(cleanTextHelper)
def cleanText(aSeries):
    return aSeries.apply(cleanTextHelper)

t_start = time()
## initialize label encoder
le = LabelEncoder()
## "category_name"
concat_train = cleanText(train.category_name)
concat_test = cleanText(test.category_name)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.category_name = le.transform(concat_train)
test.category_name = le.transform(concat_test)
## "brand_name"
concat_train = cleanText(train.brand_name)
concat_test = cleanText(test.brand_name)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.brand_name = le.transform(concat_train)
test.brand_name = le.transform(concat_test)
## "cat_name"
concat_train = cleanText(train.cat_name)
concat_test = cleanText(test.cat_name)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.cat_name = le.transform(concat_train)
test.cat_name = le.transform(concat_test)
## "cat_name2"
concat_train = cleanText(train.cat_name2)
concat_test = cleanText(test.cat_name2)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.cat_name2 = le.transform(concat_train)
test.cat_name2 = le.transform(concat_test)
## "cat_name3"
concat_train = cleanText(train.cat_name3)
concat_test = cleanText(test.cat_name3)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.cat_name3 = le.transform(concat_train)
test.cat_name3 = le.transform(concat_test)
## "cat_name4"
concat_train = cleanText(train.cat_name4)
concat_test = cleanText(test.cat_name4)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.cat_name4 = le.transform(concat_train)
test.cat_name4 = le.transform(concat_test)
## "cat_name5"
concat_train = cleanText(train.cat_name5)
concat_test = cleanText(test.cat_name5)
le.fit(np.concatenate((concat_train, concat_test), axis=0))
train.cat_name5 = le.transform(concat_train)
test.cat_name5 = le.transform(concat_test)
del le
print("label encoding took:", time() - t_start, "seconds")


##*** add meta variables **
## define generic function(s) for meta feature extraction
def get_descr_meta(dataf):
    t_meta = time()
    tmp_df = dataf
    tmp_series = tmp_df.item_description   # extract from 'item_description'  
    # description length
    tmp_df = tmp_df.assign(desc_len = tmp_series.str.len())
    # count exclamations, [!]
    tmp_df = tmp_df.assign(desc_tot_excla = tmp_series.str.count("[!]"))
    # count question marks, [?]
    tmp_df = tmp_df.assign(desc_tot_qmark = tmp_series.str.count("[?]"))
    # count alpha numeric characters, [A-Za-z0-9_]
    tmp_df = tmp_df.assign(desc_tot_alnum = tmp_series.str.count("\\w"))
    # count upper-case charcters, [A-Z]
    tmp_df = tmp_df.assign(desc_tot_upper = tmp_series.str.count("[A-Z]"))
    # count lower-case characters, [a-z]
    tmp_df = tmp_df.assign(desc_tot_lower = tmp_series.str.count("[a-z]"))
    # count digits, [0-9]
    tmp_df = tmp_df.assign(desc_tot_digit = tmp_series.str.count("[0-9]"))
    # count white space characters, [' ']
    tmp_df = tmp_df.assign(desc_tot_space = tmp_series.str.count("[' ']"))
    # count hyphens, [-]
    tmp_df = tmp_df.assign(desc_tot_hyphen = tmp_series.str.count("[-]"))
    # count words with >= 3 characters, [A-Za-z]{3,}
    tmp_df = tmp_df.assign(desc_tot_word = tmp_series.str.count("[A-Za-z]{3,}"))
    # count '[rm]'
    tmp_df = tmp_df.assign(desc_tot_rm = tmp_series.str.count("'[rm]'"))
    print("meta features from description took:", time() - t_meta, "seconds")
    return tmp_df

def get_name_meta(dataf):
    t_meta = time()
    tmp_df = dataf
    tmp_series = tmp_df.name   # extract from 'name'    
    # description length
    tmp_df = tmp_df.assign(name_len = tmp_series.str.len())
    # count exclamations, [!]
    tmp_df = tmp_df.assign(name_tot_excla = tmp_series.str.count("[!]"))
    # count question marks, [?]
    tmp_df = tmp_df.assign(name_tot_qmark = tmp_series.str.count("[?]"))
    # count alpha numeric characters, [A-Za-z0-9_]
    tmp_df = tmp_df.assign(name_tot_alnum = tmp_series.str.count("\\w"))
    # count upper-case charcters, [A-Z]
    tmp_df = tmp_df.assign(name_tot_upper = tmp_series.str.count("[A-Z]"))
    # count lower-case characters, [a-z]
    tmp_df = tmp_df.assign(name_tot_lower = tmp_series.str.count("[a-z]"))
    # count digits, [0-9]
    tmp_df = tmp_df.assign(name_tot_digit = tmp_series.str.count("[0-9]"))
    # count white space characters, [' ']
    tmp_df = tmp_df.assign(name_tot_space = tmp_series.str.count("[' ']"))
    # count hyphens, [-]
    tmp_df = tmp_df.assign(name_tot_hyphen = tmp_series.str.count("[-]"))
    # count words with >= 3 characters, [A-Za-z]{3,}
    tmp_df = tmp_df.assign(name_tot_word = tmp_series.str.count("[A-Za-z]{3,}"))
    # count '[rm]'
    tmp_df = tmp_df.assign(name_tot_rm = tmp_series.str.count("'[rm]'"))
    print("meta features from name took:", time() - t_meta, "seconds")
    return tmp_df

## meta features from 'item_description'
train = get_descr_meta(train)
test = get_descr_meta(test)
## meta features from 'name'
train = get_name_meta(train)
test = get_name_meta(test)


## print and check what things look like in both train & test
print("*** Train **")
print("features:\n", list(train.columns.values))
print("\nfirst row:\n", list(train.loc[1,:].values))
print('*' * 78)
print("*** Test **")
print("features:\n", list(test.columns.values))
print("\nfirst row:\n", list(test.loc[1,:].values))
print('*' * 78)
print("train shape:", train.shape)
print("test shape:", test.shape)